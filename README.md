#Platzom

Hi! My first project in Platzi [Platzi](https://platzi.com/js)

-Si la palabra termina en "ar", se le quitan esos dos caracteres.
-Si la palabra incia con Z se le añade "pe" al final.
-Si la palabra traducida tiene 10 o mas letras se debe partir en la mitad y unir con un guion del medio
-Si la palabra original es un palidromo, ninguna regla anterior cuenta y devuelve. La misma palabra intercalando mayusculas y minusculas.

##Instalación

```
npm install platzom
```

## Uso

```
import platzom from 'platzom'

platzom("Programar") //Program
platzom("Zorro") //Zorrope
platzom("Zarpar") //Zarppe
platzom("abecedario") //abece-dario
platzom("sometemos") //SoMeTeMoS
```

## Créditos
- [Jonathan Vega](https://gitlab.com/mdlJavaScripts/platzom)

## License

[MIT](https://opensource.org/licenses/MIT)